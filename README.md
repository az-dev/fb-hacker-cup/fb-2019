# Facebook Hacker Cup 2019

<hr />

[Facebook Hacker Cup 2019](https://www.facebook.com/codingcompetitions/hacker-cup/2019)


[Facebook Hacker Cup 2019](https://www.facebook.com/codingcompetitions/hacker-cup/2019) is solved using [PHP](https://www.php.net/).

<hr />

| Round | Problem | Comments |
|-------|---------|----------|
|       |         |          |

## License

MIT License

Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
